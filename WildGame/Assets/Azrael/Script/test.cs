using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    float m_MaxDistance;
    bool m_HitDetect;

    Collider m_Collider;
    RaycastHit m_Hit;

    void Start()
    {
        //Choose the distance the Box can reach to
        m_MaxDistance = 300.0f;
        m_Collider = GetComponent<Collider>();
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        m_HitDetect = Physics.BoxCast(m_Collider.bounds.center, transform.localScale, -transform.forward, out m_Hit, transform.rotation, m_MaxDistance);
        if (m_HitDetect)
        {
            Debug.Log("Hit : " + m_Hit.collider.name);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (m_HitDetect)
        {
            Gizmos.DrawRay(transform.position, -transform.forward * m_Hit.distance);

            Gizmos.DrawWireCube(transform.position + -transform.forward * m_Hit.distance, transform.localScale);
        }

        else
        {
            Gizmos.DrawRay(transform.position, -transform.forward * m_MaxDistance);

            Gizmos.DrawWireCube(transform.position + -transform.forward * m_MaxDistance, transform.localScale);
        }
    }
}
