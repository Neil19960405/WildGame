﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject target;
    private Camera camera;
    private bool isFirstPersonPerspective;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("target");
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // v鍵控制視角
        if (isFirstPersonPerspective)
        {
            FirstPersonPerspective();

            if (Input.GetKeyDown("v"))
                isFirstPersonPerspective = false;
        }
        else
        {
            ThirdPersonPerspective();

            if (Input.GetKeyDown("v"))
                isFirstPersonPerspective = true;
        }
    }
    /// <summary>
    /// 第一人稱視角
    /// </summary>
    private void FirstPersonPerspective()
    {
        camera.transform.position = target.transform.position;
        camera.transform.forward = target.transform.forward;
    }
    /// <summary>
    /// 第三人稱視角
    /// </summary>
    private void ThirdPersonPerspective()
    {
        Vector3 cPos = camera.transform.position;
        Vector3 tPos = target.transform.position;
        Vector3 vDis = tPos - cPos;
        float speed;

        float dis = vDis.magnitude;
        vDis.y = 0;

        // 視角切換彈開鏡頭
        if (dis < 0.0000001f)
        {
            camera.transform.position = cPos - tPos;
        }

        // 鏡頭離腳色多遠
        if (dis > 3)
        {
            speed = 2.0f;
        }
        else if (dis < 2.9)
        {
            speed = -2.0f;
        }
        else
        {
            speed = 0.0f;
        }

        // 鏡頭左右平移
        if (Input.GetKey("q"))
        {
            camera.transform.position += transform.right * 5.0f * -1 * Time.deltaTime;
        }
        else if (Input.GetKey("e"))
        {
            camera.transform.position += transform.right * 5.0f * 1 * Time.deltaTime;
        }

        // 鏡頭跟隨
        camera.transform.position += vDis * speed * Time.deltaTime;
        camera.transform.LookAt(tPos);

        // 行走時鏡頭導正至背後
        camera.transform.RotateAround(tPos, Vector3.up, Input.GetAxis("Horizontal"));

        // 滑鼠右鍵轉鏡頭角度
        if (Input.GetMouseButton(1))
        {
            camera.transform.RotateAround(tPos, Vector3.up, Input.GetAxis("Mouse X") * 5);
            camera.transform.RotateAround(tPos, Vector3.right, Input.GetAxis("Mouse Y") * 5);
        }
    }
}