using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    private float move;
    private float turn;
    private float jump;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        move = Input.GetAxis("Vertical");
        turn = Input.GetAxis("Horizontal");
        jump = Input.GetAxis("Jump");

        // OićŽ
        transform.position += transform.forward * 5.0f * move * Time.deltaTime;
        // ēzü
        transform.Rotate(new Vector3(0, turn, 0));

        // µō
        if (Input.GetKeyDown("space") && transform.position.y < 0.0000001f)
        {
            GetComponent<Rigidbody>().velocity += new Vector3(0, 5, 0);
            GetComponent<Rigidbody>().AddForce(Vector3.up * jump);
        }

        // ¶E½Ś
        if (Input.GetKey("q"))
        {
            transform.position += transform.right * 5.0f * -1 * Time.deltaTime;
        }
        else if (Input.GetKey("e"))
        {
            transform.position += transform.right * 5.0f * 1 * Time.deltaTime;
        }
    }
}